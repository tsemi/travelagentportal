<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rooms
 *
 * @ORM\Table(name="rooms")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoomsRepository")
 */
class Rooms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float")
     */
    private $rate;


    /**
     * @var float
     *
     * @ORM\Column(name="cp_cost", type="float")
     */
    private $cp_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="ep_cost", type="float")
     */
    private $ep_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="ap_cost", type="float")
     */
    private $ap_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="map_cost", type="float")
     */
    private $map_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="extra_bed", type="float")
     */
    private $extra_bed;

    /**
     * @var int
     *
     * @ORM\Column(name="adult", type="integer")
     */
    private $adult;

    /**
     * @var int
     *
     * @ORM\Column(name="max_person", type="integer")
     */
    private $maxPerson;

    /**
     * @var int
     *
     * @ORM\Column(name="room_count", type="integer")
     */
    private $roomCount;


    /**
     * @var bool
     *
     * @ORM\Column(name="smoking", type="boolean")
     */
    private $smoking;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     * @Assert\File(
     *     maxSize = "100k",
     *     mimeTypes = {"image/jpg","image/jpeg"},
     *     mimeTypesMessage = "Please upload a valid image"
     * )
     * @ORM\Column(name="imageurl", type="string", length=255)
     */
    private $imageurl;


    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Images", mappedBy="rooms",cascade={"all"},orphanRemoval=true)
     */
    private $image;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Property", inversedBy="rooms")
     *)
     */
    private $property;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reservation", mappedBy="room")
     */
    private $reservation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Rooms
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Rooms
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return float
     */
    public function getCpCost()
    {
        return $this->cp_cost;
    }

    /**
     * @param float $cp_cost
     */
    public function setCpCost($cp_cost)
    {
        $this->cp_cost = $cp_cost;
    }

    /**
     * @return float
     */
    public function getEpCost()
    {
        return $this->ep_cost;
    }

    /**
     * @param float $ep_cost
     */
    public function setEpCost($ep_cost)
    {
        $this->ep_cost = $ep_cost;
    }

    /**
     * @return float
     */
    public function getApCost()
    {
        return $this->ap_cost;
    }

    /**
     * @param float $ap_cost
     */
    public function setApCost($ap_cost)
    {
        $this->ap_cost = $ap_cost;
    }

    /**
     * @return float
     */
    public function getMapCost()
    {
        return $this->map_cost;
    }

    /**
     * @param float $map_cost
     */
    public function setMapCost($map_cost)
    {
        $this->map_cost = $map_cost;
    }

    /**
     * @return float
     */
    public function getExtraBed()
    {
        return $this->extra_bed;
    }

    /**
     * @param float $extra_bed
     */
    public function setExtraBed($extra_bed)
    {
        $this->extra_bed = $extra_bed;
    }


    /**
     * Set adult
     *
     * @param integer $adult
     *
     * @return Rooms
     */
    public function setAdult($adult)
    {
        $this->adult = $adult;

        return $this;
    }

    /**
     * Get adult
     *
     * @return int
     */
    public function getAdult()
    {
        return $this->adult;
    }

    /**
     * @return int
     */
    public function getMaxPerson()
    {
        return $this->maxPerson;
    }

    /**
     * @param int $maxPerson
     */
    public function setMaxPerson($maxPerson)
    {
        $this->maxPerson = $maxPerson;
    }



    /**
     * @return int
     */
    public function getRoomCount()
    {
        return $this->roomCount;
    }

    /**
     * @param int $roomCount
     */
    public function setRoomCount($roomCount)
    {
        $this->roomCount = $roomCount;
    }



    /**
     * Set smoking
     *
     * @param boolean $smoking
     *
     * @return Rooms
     */
    public function setSmoking($smoking)
    {
        $this->smoking = $smoking;

        return $this;
    }

    /**
     * Get smoking
     *
     * @return bool
     */
    public function getSmoking()
    {
        return $this->smoking;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Rooms
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set imageurl
     *
     * @param string $imageurl
     *
     * @return Rooms
     */
    public function setImageurl($imageurl)
    {
        $this->imageurl = $imageurl;
        return $this;
    }
    /**
     * Get imageurl
     *
     * @return string
     */
    public function getImageurl()
    {
        return $this->imageurl;
    }


    /**
     * Set property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return Rooms
     */
    public function setproperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \AppBundle\Entity\Property
     */
    public function getproperty()
    {
        return $this->property;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     *
     * @return Rooms
     */
    public function addReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Get reservation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    public function __toString()
    {
        return $this->getType();
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Rooms
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->image[] = $image;
        $image->setRooms($this);
        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }
}
