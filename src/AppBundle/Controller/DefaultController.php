<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Images;
use AppBundle\Entity\Property;
use AppBundle\Entity\Reservation;
use AppBundle\Entity\Rooms;
use AppBundle\Entity\User;
use AppBundle\Form\ReservationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/home", name="landing")
     */
    public function landingAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/landing.html.twig');
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {
        return $this->render(':default:dashboard.html.twig');
    }


    /**
     * @Route("/property-list", name="porperty_list")
     */
    public function propertyListAction(Request $request)
    {
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $em = $this->getDoctrine()->getManager();
        $locations = $em->getRepository('AppBundle:Property')->findAll();
        $res=array();
        foreach ($locations as $var)
        {
            $id = $var->getId();
            $name = $var->getName();
            $room = $var->getRooms();
            foreach ($room as $item) {
                if($item instanceof Rooms)
                {
                    $room=$item->getType();
                }
            }

            $res[]= array('id'=>$id,'name'=>$name,'room'=>$room);
        }
        return $this->render(':default:search-form.html.twig', array(
            'res'=>$res,
            'postData'=>$filter
        ));
    }


    /**
     * @Route("/search/result", name="search")
     */
    public function searchAction(Request $request)
    {
        $session = $request->getSession();
        $session->set('postData', $request->request->all());
        $id = $request->request->get('destination');
        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository('AppBundle:Property')->filterRoomsAction();
        $detail = array();
        $property = $em->getRepository('AppBundle:Property')->findBy(array('name'=>$id));
        $photos = $em->getRepository('AppBundle:Rooms')->findAll();
        $res =array();
        foreach($photos as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                $id= $value->getId();
                $detail = array();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $detail[] = $val->getPath();
                    }
                }
                $response[] = array('images' => $detail,'id'=>$id);
            }
        }
        foreach($property as $val)
        {
            $id = $val->getId();
            $name = $val->getName();
            $desc = $val->getDescription();
            $image = $val->getImage();
            foreach ($image as $images)
            {
                if ($images instanceof Images)
                {
                    $detail[] = array('path' => $images->getPath());
                }
            }
            $res[]= array('id'=>$id,'name'=>$name,'desc'=>$desc,
                'images'=>$detail);
        }
        if ($request->isMethod('POST')) {
            $session->remove('cart');
        }
        return $this->render('default/search-result.html.twig', array(
            'session'=> $session->get('postData'),
            'response'=>$rooms,
            'res'=>$res,
            'images'=>$response,
        ));
    }




//    /**
//     * @Route("/rooms-list", name="rooms_data")
//     */
//    public function RoomSearchAction(Request $request)
//    {
//        $session = $request->getSession();
//        $em = $this->getDoctrine()->getManager();
//        $rooms = $em->getRepository('AppBundle:Rooms')->filterRoomsAction($request);
//        return $this->render(':default:rooms-list.html.twig', array(
//            'session'=> $session->get('postData'),
//            'response'=>$rooms,
//        ));
//    }


    /**
     * @Route("/booking", name="booking")
     */
    public function BookingAction(Request $request)
    {
        $userGrade = $this->getUser()->getCategory();
        $userId = $this->getUser()->getID();
        $email = $this->getUser()->getEmail();
        $userName = $this->getUser()->getFirstName();
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $cart=$session->get('cart',array());
        $form = $this->createFormBuilder()
            ->add('firstname', TextType::class, array(
                'required' => false,
            ))
            ->add('lastname', TextType::class, array(
                'required' => false,
            ))
            ->add('phone', TextType::class, array(
                'required' => false,
                'attr'=> array(
                    'min' => '10',
                    'max' => '10',
                    'maxlength' => 10
                ),
     'constraints' => array(
        new Assert\Type('digit'),
        new Assert\Regex(array(
                'pattern' => '/^[0-9]{10}$/',
                'message' => 'Please use valid mobile number'
            )
        ),
        new Assert\Length(array('min' => 10,'max' => 10))
    )
            ))
            ->add('address', TextType::class, array(
                'required' => false,
            ))
            ->add('message', TextareaType::class, array(
                'label'=>'Any special request',
                'required' => false,
            ))
            ->add('terms', CheckboxType::class, array(
                'required' => true,

            ))
            ->add('amount', TextType::class, array(
                'required' => false,
            ))
            ->add('amountA', TextType::class, array(
                'required' => false,
            ))
            ->getForm();
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $res =array();
        foreach($cart as $val)
        {
            $id = $val['id'];
            $name = $val['type'];
            $plan = $val['plan'];
            $price = $val['rate'];
            $cp = $val['cp'];
            $ep = $val['ep'];
            $ap = $val['ap'];
            $map = $val['map'];
            $bed = $val['bed'];
            $img = $val['imageurl'];
            $adult = $val['adult'];
            $room_adult = $val['room_adult'];
            $quantity = $val['quantity'];
            $total = $val['total'];
            $res[]= array('id'=>$id,'name'=>$name,'price'=>$price,'plan'=>$plan,'img'=>$img,'cp'=>$cp,'ep'=>$ep,'ap'=>$ap,'map'=>$map,'bed'=>$bed,'adult'=>$adult,'room_adult'=>$room_adult,'quantity'=>$quantity,'total'=>$total );
        }
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($cart as $item) {
                $reservation = new Reservation();
                $reservation->setRoom($em->getRepository('AppBundle:Rooms')->find($item['id']));
                $reservation->setPlan($item['plan']);
                $reservation->setGuests($item['room_adult']);
                $reservation->setStatus(0);
                $reservation->setUser($em->getRepository('AppBundle:User')->find($userId));
                $reservation->setRoomBooked($item['quantity']);
                $reservation->setCheckIn(\DateTime::createFromFormat('d/m/Y', $filter['checkin']));
                $reservation->setCheckOut(\DateTime::createFromFormat('d/m/Y', $filter['checkout']));
                $reservation->setCreated(new \DateTime("now"));
                $em->persist($reservation);
                $em->flush();
            }
            $fname = $form['firstname']->getData();
            $lastname = $form['lastname']->getData();
            $gPhone = $form['phone']->getData();
            $gaddress = $form['address']->getData();
            $message = $form['message']->getData();
            $amount = $form['amount']->getData();
            $amountA = $form['amountA']->getData();
            if($userGrade == 1){
                $gA = 20;
            }elseif($userGrade == 2){
                $gA = 15;
            }elseif($userGrade == 3){
                $gA = 10;
            }else{
                $gA = 0;
            }
            $html1 = "<div style='background: #D4EFDF; box-shadow: 5px 5px 5px #000; border: 1px dashed #A9DFBF; padding: 40px 30px'> 
             <strong>Dear Admin,</strong><br>
                        <br>Room Booking Enquiry From $email<br><br>";
            $html1 .="<table>
                <thead>
                    <tr>
                        <th align='left' width='250px'>Room Details</th>
                        <th align='left'>Guest Details</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                <td valign='top'>
                <strong>Check In Date:</strong> {$filter['checkin']} <br>
                        <strong>Check Out Date:</strong> {$filter['checkout']}<br>";
            $html1 .="<br><strong>Property: </strong>{$filter['destination']}<br>";
            $html1 .="<br><strong>Room Detail: </strong> {$item['plan']}<br> ";
            foreach ($cart as $item) {
                $html1 .= "{$item['quantity']}  {$item['type']} : {$item['total']}<br> ";
            }
            $html1 .="<br><strong>Guests: </strong><br> ";
            foreach ($cart as $item) {
                $html1 .= "{$item['type']} - {$item['room_adult']} <br> ";
            }
            $html1 .= "
<br><strong>Sub Total:</strong> $amountA
<br><strong>Discount:</strong> $gA% 
<br><strong>Total:</strong> $amount<br><br>
</td>
<td valign='top'>
<strong>Name: </strong> $fname $lastname <br>
                        <strong>Phone No.: </strong> $gPhone<br>
                        <strong>Address: </strong> $gaddress<br>
</td>
</tr>


</tbody>
</table>";
            $html1 .= "<strong>Special Requirement : </strong>$message </div>";
            $html = "<div style='background: #D4EFDF; box-shadow: 5px 5px 5px #000; border: 1px dashed #A9DFBF; padding: 40px 30px'> 
             <strong>Dear $userName,</strong><br>
                        <br>This email is to confirm your booking with Ourguest.in<br><br>";
            $html .="<table>
                <thead>
                    <tr>
                        <th align='left' width='250px'>Room Details</th>
                        <th align='left'>Guest Details</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                <td valign='top'>
                <strong>Check In Date:</strong> {$filter['checkin']} <br>
                        <strong>Check Out Date:</strong> {$filter['checkout']}<br>";

            $html.="<br><strong>Property: </strong>{$filter['destination']}<br>";
            $html.="<br><strong>Room Detail: </strong> {$item['plan']}<br> ";
            foreach ($cart as $item) {
                $html.= "{$item['quantity']}  {$item['type']} : {$item['total']}<br> ";
            }
            $html.="<br><strong>Guests: </strong><br> ";
            foreach ($cart as $item) {
                $html.= "{$item['type']} - {$item['room_adult']} <br> ";
            }
            $html .= "
            <br><strong>Sub Total:</strong> $amountA
<br><strong>Discount:</strong> $gA% 
<br><strong>Total:</strong> $amount<br><br>
</td>
<td valign='top'>
<strong>Name: </strong> $fname $lastname <br>
                        <strong>Phone No.: </strong> $gPhone<br>
                        <strong>Address: </strong> $gaddress<br>
</td>
</tr>

</tbody>
</table>";

            $html .="<strong>Special Requirement : </strong>$message<br><br><span style='background: #CB4335; color: #fff; padding: 6px 8px'> Please Pay $amount for Confirmation within 72 hrs.</span><br><br>
                         The booking will be held for 72 hrs only. Kindly transfer the amount to the bank account shared below.<br><br>
                          <strong>Name:</strong> Topview Infolabs Pvt. Ltd.<br>
 <strong>A/c No. :</strong> 047005000425<br>
 <strong>Bank:</strong> ICICI Bank, <br>
 <strong>Branch:</strong> M. G. Marg, Gangtok Branch<br>
 <strong>IFSC :</strong> ICIC0000470<br><br>

Feel free to call us @97335 03347 or 97348 13101</div>";
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                ->setUsername('togglebrain9@gmail.com')
                ->setPassword('lqlvdhrbrvvosdra');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();
//                $header = $message->embed(\Swift_Image::fromPath('CAT2/images/header.jpg'));
            $message->setSubject('Booking Confirmation')
                ->setFrom(array("a@gmail.com"=>"Ourguest.in"))
                ->setTo(array(
                    $email=>'Email Owner'))
                ->setBody($html, 'text/html');

            $mailer->send($message);

            $message1 = \Swift_Message::newInstance();
//                $header = $message->embed(\Swift_Image::fromPath('CAT2/images/header.jpg'));
            $message1->setSubject('Room Booking')
                ->setFrom(array("contact@ourguest.in"=>"Ourguest.in"))
                ->setReplyTo(array(
                    $email=>$userName))
                ->setTo(array(
                    'ezycreation@gmail.com'=>'Email Owner',
                    'singhisachin@gmail.com'=>'Ourguest'
                ))
                ->setBody($html1, 'text/html');

            $mailer->send($message1);


            $session->remove('cart');
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Room Booked successfully, kindly check your email for payment details. !');
            return $this->redirectToRoute('booking');

        }
        return $this->render('default/reservation.html.twig', array(
            'res'=>$res,
            'postData'=>$filter,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/success", name="booking_success")
     */
    public function BookingSuccessAction(Request $request)
    {
        return $this->render('default/success.html.twig');
    }


    public function FeaturedProopertyAction(){
        $em = $this->getDoctrine()->getManager();
        $detail= array();
        $view = $em->getRepository('AppBundle:Property')->featuredProperty();
        foreach($view as $value)
        {
            if ($value instanceof Property) {
                $image = $value->getFeatimage();
                $title = $value->getName();
                $desc = $value->getDescription();
                $url = $value->getUrl();
                $id = $value->getId();
                $detail[] = array('title' =>$title,'image'=>$image,'id' => $id, 'desc'=>$desc,'url'=>$url);
            }
        }
        return $this->render(':default:featured-property.html.twig', array(
            'detail' => $detail
        ));
    }

    /**
     * @Route("/cart", name="add_cart")
     */
    public function cartProductAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $cart = $em->getRepository('AppBundle:Rooms')->findCartAction($request);
        $count = count($cart);
        return $this->render(':default:cart.html.twig', array(
            'cart' => $cart,
            'count' => $count,
            'postData'=>$filter
        ));
    }


    /**
     * @Route("/rooms-gallery", name="room_gallery")
     */
    public function RoomsGalleryAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $photos = $em->getRepository('AppBundle:Rooms')->findAll();
        foreach($photos as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                $id= $value->getId();
                $detail = array();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $detail[] = $val->getPath();
                    }
                }
                $response[] = array('images' => $detail,'id'=>$id);
            }
        }

        return $this->render('default/room-gallery.html.twig', array(
            'images'=>$response,
        ));
    }

    /**
     * @Route("/terms-and-condition", name="terms")
     */
    public function TermsConditionAction(Request $request){
        return $this->render(':default:terms-condition.html.twig');
    }

    /**
     * @Route("/contact", name="contact_us")
     */
    public function ContactAction(Request $request){
        return $this->render(':default:contact-us.html.twig');
    }

}
