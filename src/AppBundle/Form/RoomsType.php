<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoomsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type')->add('rate')->add('adult')->add('maxPerson')
//            ->add('amenities', ChoiceType::class, array(
//                'choices' => array(
//                    'Wifi' => 'fa-wifi',
//                    'Cab Service' => 'fa-cab',
//                    'Travel Desk'=>'fa-plane',
//                    'CCTV'=> 'fa-file-video-o',
//                    'Dr. On Call' => 'fa-medkit',
//                    'Power Backup' => 'fa-plug',
//                    'Laundry' => 'fa-shopping-basket',
//                    'Room Service' => 'fa-coffee',
//                    'Food' => 'fa-cutlery',
//                    'Casino' => 'fa-support',
//                    'AC'    => 'fa-snowflake-o',
//                    'Lift' => 'fa-sort-numeric-asc',
//                    'Parking' => 'fa-product-hunt',
//                    'Bar' => 'fa-glass',
//                    'Swimming Pool' => 'fa-stumbleupon'
//
//                ),
//                'multiple' => true,
//                'expanded' => true
//            ))
            ->add('smoking',ChoiceType::class,array(
                'choices'=>array(
                    'Yes'=> true,
                    'No'=> false
                )
            ))
            ->add('description')
            ->add('property')
            ->add('cp_cost',TextType::class,array(
                'label'=>'CP Cost'
            ))
            ->add('ep_cost',TextType::class,array(
                'label'=>'EP Cost'
            ))
            ->add('ap_cost',TextType::class,array(
                'label'=>'AP Cost'
            ))
            ->add('map_cost',TextType::class,array(
                'label'=>'MAP Cost'
            ))
            ->add('extra_bed',TextType::class,array(
                'label'=>'Extra Bed'
            ))
            ->add('image', CollectionType::class, array(
                'entry_type' => ImagesType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'required' => false
            ))
            ->add('roomCount',TextType::class,array(
                'label'=> 'No. of Rooms'
            ))

        ->add('imageurl',FileType::class,array('label'=>'Featured Image','required'=>false));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Rooms'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rooms';
    }


}
