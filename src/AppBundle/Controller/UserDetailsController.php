<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserDetails;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Userdetail controller.
 *
 * @Route("userdetails")
 */
class UserDetailsController extends Controller
{
    /**
     * Lists all userDetail entities.
     *
     * @Route("/", name="userdetails_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userDetails = $em->getRepository('AppBundle:UserDetails')->findAll();

        return $this->render('userdetails/index.html.twig', array(
            'userDetails' => $userDetails,
        ));
    }

    /**
     * Creates a new userDetail entity.
     *
     * @Route("/new", name="userdetails_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $userId = $this->getUser()->getID();
        $userDetail = new UserDetails();
        $form = $this->createForm('AppBundle\Form\UserDetailsType', $userDetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userDetail);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Thank You for your time !');
            return $this->redirectToRoute('landing');
        }

        return $this->render('userdetails/new.html.twig', array(
            'userId'=>$userId,
            'userDetail' => $userDetail,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a userDetail entity.
     *
     * @Route("/{id}", name="userdetails_show")
     * @Method("GET")
     */
    public function showAction(UserDetails $userDetail)
    {
        $deleteForm = $this->createDeleteForm($userDetail);

        return $this->render('userdetails/show.html.twig', array(
            'userDetail' => $userDetail,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing userDetail entity.
     *
     * @Route("/{id}/edit", name="userdetails_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, UserDetails $userDetail)
    {
        $deleteForm = $this->createDeleteForm($userDetail);
        $editForm = $this->createForm('AppBundle\Form\UserDetailsType', $userDetail);
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $editForm->remove('user');
        }
        else{
            $editForm->remove('user');
//            $editForm->remove('message');
//            $editForm->remove('source');
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'User details updated successfully !');
            return $this->redirectToRoute('userdetails_edit', array('id' => $userDetail->getId()));
        }

        return $this->render('userdetails/edit.html.twig', array(
            'userDetail' => $userDetail,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a userDetail entity.
     *
     * @Route("/{id}", name="userdetails_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UserDetails $userDetail)
    {
        $form = $this->createDeleteForm($userDetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($userDetail);
            $em->flush();
        }

        return $this->redirectToRoute('userdetails_index');
    }

    /**
     * Creates a form to delete a userDetail entity.
     *
     * @param UserDetails $userDetail The userDetail entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserDetails $userDetail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('userdetails_delete', array('id' => $userDetail->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
